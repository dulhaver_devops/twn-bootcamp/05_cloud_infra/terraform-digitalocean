terraform {
  required_version = ">= 1.0.0"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# variables ####################################################################

variable "playbook" {}
variable "ssh_key_name" {}

locals {
  ssh_user      = "sysop"
  key_name      = "dulhaver_devops"
  pvt_key 		= "~/.ssh/id_rsa_twn"
}

# provider #####################################################################
provider "digitalocean" {}


# resource #####################################################################
data "digitalocean_ssh_key" "personal" {
  name = var.ssh_key_name
  # name = "id_rsa_twn.pub"
}

resource "digitalocean_droplet" "module-5-cloud-infra" {
  image     = "debian-12-x64"
  name      = "module-5-cloud-infra"
  region    = "fra1"
  size      = "s-2vcpu-8gb-amd"	# "s-1vcpu-1gb"	 # "s-1vcpu-512mb-10gb"	# 
  ssh_keys  = [data.digitalocean_ssh_key.personal.id]
  user_data = file("./cloud.yml")


# #############################################################################
# https://github.com/antonputra/tutorials/blob/main/lessons/014/main.tf
# #############################################################################
provisioner "remote-exec" {
    inline	= ["echo 'wait until ssh is ready for Ansible'"]
	connection {
	  type			= "ssh"
	  user			= local.ssh_user
	  private_key	= file(local.pvt_key)
	  host			= self.ipv4_address
	}
  }	
# local-exec ###################################################################
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u sysop -i '${self.ipv4_address},' --private-key ${local.pvt_key} ${var.playbook}"
	}
}
