
resource "digitalocean_firewall" "module-06-nexus-in" {
  name = "module-06-nexus-in"

  droplet_ids = [digitalocean_droplet.module-5-cloud-infra.id]

# ### inbound rules ###########################################################
  inbound_rule {
    protocol         = "tcp"
    port_range       = "8081"
    source_addresses = [ var.public_ip ]
  }
# ### web servers ######################
#   inbound_rule {
#     protocol         = "tcp"
#     port_range       = "80"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }
# 
#   inbound_rule {
#     protocol         = "tcp"
#     port_range       = "443"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }

# # ### icmp #############################
#   inbound_rule {
#     protocol         = "icmp"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }
# 
# # ### node app #########################
#   inbound_rule {
#     protocol         = "tcp"
# 	port_range		 = "3000"
#     source_addresses = ["0.0.0.0/0", "::/0"]
#   }
# 
# # ### outbound rules ##########################################################
# 
# # ### DNS ##############################
# #   outbound_rule {
# #     protocol              = "tcp"
# #     port_range            = "53"
# #     destination_addresses = ["0.0.0.0/0", "::/0"]
# #   }
#   outbound_rule {
#     protocol              = "udp"
#     port_range            = "53"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }
# 
# # ### icmp #############################
#   outbound_rule {
#     protocol              = "icmp"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }
# 
# # ### apt rules ########################
#   outbound_rule {
#     protocol              = "tcp"
#     port_range            = "80"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }
#   outbound_rule {
#     protocol              = "tcp"
#     port_range            = "443"
#     destination_addresses = ["0.0.0.0/0", "::/0"]
#   }

}
